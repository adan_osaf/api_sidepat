<?php

Route::get('/', function () {
    $app_name = config('app.name');
    $app_version = app()->version();
    return "${app_name} by Órgano Superior de Auditoría y Fiscalización Gubernamental | Laravel Framework ${app_version}";
});

Route::get('/login', function() {
    return response()->json([
       'message' => 'Unauthorized'
    ], 401);
})->name('login');
